[Home](home)

## 常用路径
> 带*表示通过Homebrew安装

- Java Home: `/Library/Java/JavaVirtualMachines/jdk1.8.0_161.jdk/Contents/Home`
- Maven Home: `/usr/local/maven`
- Tomcat Home: `/usr/local/tomcat/apache-tomcat-8.5.27`
- npm全局模块安装路径: `/usr/local/lib/node_modules`
- mysql: `/usr/local/mysql`
- git: `/usr/bin/git`
- zsh: `/bin/zsh`
- brew配置文件地址: `/usr/local/etc`
- *activemq安装路径: ``
- *mongodb: `/usr/local/Cellar/mongodb/`
- *nginx: `/usr/local/Cellar/nginx/`
- *redis: `/usr/local/Cellar/redis/`
- *Gradle Home: `/usr/local/Cellar/gradle/4.6`